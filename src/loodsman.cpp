// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

/*
(C) 2020 ������ �������

������������� �������� �������� Apache, ������ 2.0 ("��������");
�� ������ ������������ ���� ���� ������ � ������������ � ���������.
�� ������ ����� ����� �������� �� ������

http://www.apache.org/licenses/LICENSE-2.0

�� ����������� �������, ����� ��� ���������������� ������������
����������������� ��� ���� ��� �� ��������� � ���������� ����������,
����������� ����������� ���������������� �� �������� ������ ��������,
��������������� "��� ����" � ����� ����� ��� ������� �������� �����������.
���������� �� �������� ������ � ������������, ����������� � �������������
����� �������� ��������, �� ������ ����� � ������ ��������.
*/

#include "stdafx.h"
#include "config.h"

#include <boost/python.hpp>
#include <boost/python/manage_new_object.hpp>
#include <boost/python/manage_new_object.hpp>
#include "connection.h"
#include "dataset.h"
#include <loodsman/Connection.h>

namespace p = boost::python;

void translate_oleerror(oleerror const& e)
{
    PyErr_SetString(PyExc_OSError, CW2A(e.what(), CP_UTF8));
}

void translate_connection_error(loodsman::Connection::Error const& e)
{
    PyErr_SetString(PyExc_ConnectionError, CW2A(e.what(), CP_UTF8));
}

std::string version_str()
{
    return std::string(PROJECT_VERSION);
}

boost::python::tuple version()
{
    return boost::python::make_tuple(PROJECT_VERSION_MAJOR, PROJECT_VERSION_MINOR, PROJECT_VERSION_PATCH, PROJECT_VERSION_TWEAK);
}

BOOST_PYTHON_MODULE(loodsman)
{
    p::class_<DatasetWrapper>("Dataset", p::init<>())
        .def("First", &DatasetWrapper::First)
        .def("Last", &DatasetWrapper::Last)
        .def("Next", &DatasetWrapper::Next)
        .def("Prior", &DatasetWrapper::Prior)
        .def("Append", &DatasetWrapper::Append)
        .def("Delete", &DatasetWrapper::Delete)
        .def("Edit", &DatasetWrapper::Edit)
        .def("Post", &DatasetWrapper::Post)
        .def("Cancel", &DatasetWrapper::Cancel)
        .def("Insert", &DatasetWrapper::Insert)
        .def("MoveBy", &DatasetWrapper::MoveBy)
        .def("Locate", &DatasetWrapper::Locate)
        .def("Clear", &DatasetWrapper::Clear)
        .def("Locate2", &DatasetWrapper::Locate2)
        .def("__getitem__",&DatasetWrapper::get_FieldValue)
        .def("__setitem__", &DatasetWrapper::put_FieldValue)
        .def("keys", &DatasetWrapper::get_FieldNames)
        .def("__str__", &DatasetWrapper::toStr)
        .def("__len__", &DatasetWrapper::get_RecordCount)
        .def("__enter__", &DatasetWrapper::enter)
        .def("__exit__", &DatasetWrapper::exit)
        //.def("get_FieldName", &DatasetWrapper::get_FieldName)
        .def("toList", &DatasetWrapper::toList)
        .add_property("Eof", &DatasetWrapper::get_Eof)
        .add_property("Bof", &DatasetWrapper::get_Bof)
        .add_property("IndexFieldNames", &DatasetWrapper::get_IndexFieldNames, &DatasetWrapper::put_IndexFieldNames)
        .add_property("CurrentRecord", &DatasetWrapper::get_CurrentRecord, &DatasetWrapper::put_CurrentRecord)
        .add_property("RecordCount", &DatasetWrapper::get_RecordCount)
        .add_property("FieldCount", &DatasetWrapper::get_FieldCount)
        .add_property("FieldNames", &DatasetWrapper::get_FieldNames)
        .add_property("Filter", &DatasetWrapper::get_Filter, &DatasetWrapper::put_Filter)
        .add_property("Filtered", &DatasetWrapper::get_Filtered, &DatasetWrapper::put_Filtered)
        .add_property("values", &DatasetWrapper::get_FieldValues)
        ;

    p::class_<ConnectionWrapper>("Connection", p::init<const std::string &>())
        .def("RunMethod", &ConnectionWrapper::RunMethod)
        ;

    p::def("version_str", version_str);
    p::def("version", version);

    p::register_exception_translator<oleerror>(translate_oleerror);
    p::register_exception_translator<loodsman::Connection::Error>(translate_connection_error);
}



