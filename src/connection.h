// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

/*
(C) 2020 ������ �������

������������� �������� �������� Apache, ������ 2.0 ("��������");
�� ������ ������������ ���� ���� ������ � ������������ � ���������.
�� ������ ����� ����� �������� �� ������

http://www.apache.org/licenses/LICENSE-2.0

�� ����������� �������, ����� ��� ���������������� ������������
����������������� ��� ���� ��� �� ��������� � ���������� ����������,
����������� ����������� ���������������� �� �������� ������ ��������,
��������������� "��� ����" � ����� ����� ��� ������� �������� �����������.
���������� �� �������� ������ � ������������, ����������� � �������������
����� �������� ��������, �� ������ ����� � ������ ��������.
*/

#pragma once

#include <loodsman/Connection.h>

#include <boost/python/def.hpp>
#include <boost/python.hpp>

namespace p = boost::python;

class ConnectionWrapper
{
    loodsman::Connection* mpConnection;
public:
    ConnectionWrapper(const std::string& host);
    void del();
    boost::python::object RunMethod(const std::string& name, const boost::python::list& args);
};