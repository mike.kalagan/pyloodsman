// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

/*
(C) 2020 ������ �������

������������� �������� �������� Apache, ������ 2.0 ("��������");
�� ������ ������������ ���� ���� ������ � ������������ � ���������.
�� ������ ����� ����� �������� �� ������

http://www.apache.org/licenses/LICENSE-2.0

�� ����������� �������, ����� ��� ���������������� ������������
����������������� ��� ���� ��� �� ��������� � ���������� ����������,
����������� ����������� ���������������� �� �������� ������ ��������,
��������������� "��� ����" � ����� ����� ��� ������� �������� �����������.
���������� �� �������� ������ � ������������, ����������� � �������������
����� �������� ��������, �� ������ ����� � ������ ��������.
*/

#include "stdafx.h"
#include "config.h"
#include "connection.h"
#include "dataset.h"

#include "util.h"

ConnectionWrapper::ConnectionWrapper(const std::string& host)
{
    mpConnection = new loodsman::Connection(CString(CA2W(host.c_str())));
}

void ConnectionWrapper::del()
{
    delete mpConnection;
}

boost::python::object ConnectionWrapper::RunMethod(const std::string& name, const boost::python::list& args)
{
    std::vector<CComVariant> varargs;

    for (int i = 0; i < boost::python::len(args); ++i) {
        varargs.emplace_back(PYOBJ2VAR(args[i]));
    }

    CComVariant retval;
    CComBSTR bsName(name.c_str());
    mpConnection->Invoke(bsName, varargs, &retval);
    if (retval.vt == VT_DISPATCH) {
        CComQIPtr<DataProvider::IDataSet> pDP = retval.pdispVal;
        retval.pdispVal->Release();
        if (pDP == nullptr) {
            throw std::exception(CW2A(L"Wrong data set", CP_UTF8));
        }
        return p::object(new DatasetWrapper(pDP));
    } else {
        return VAR2PYOBJ(retval);
    }
}
