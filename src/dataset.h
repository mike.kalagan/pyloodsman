// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

/*
(C) 2020 ������ �������

������������� �������� �������� Apache, ������ 2.0 ("��������");
�� ������ ������������ ���� ���� ������ � ������������ � ���������.
�� ������ ����� ����� �������� �� ������

http://www.apache.org/licenses/LICENSE-2.0

�� ����������� �������, ����� ��� ���������������� ������������
����������������� ��� ���� ��� �� ��������� � ���������� ����������,
����������� ����������� ���������������� �� �������� ������ ��������,
��������������� "��� ����" � ����� ����� ��� ������� �������� �����������.
���������� �� �������� ������ � ������������, ����������� � �������������
����� �������� ��������, �� ������ ����� � ������ ��������.
*/

#pragma once
#include <boost/python.hpp>
#include <loodsman/Connection.h>
#include <string>
#include <vector>

#define PYOLECHECK(statement) \
try {\
    if (mpDataset == nullptr) { \
        PyErr_SetString(PyExc_RuntimeError, CW2A(L"Empty data set", CP_UTF8)); \
    }\
    HRESULT hr = statement;\
    if (FAILED(hr)) throw oleerror(hr, #statement);\
}\
catch (const oleerror& e) {\
    PyErr_SetString(PyExc_RuntimeError, CW2A(e.what(), CP_UTF8));\
}


class DatasetWrapper
{
    /// \todo ������� ��������
    CComPtr<DataProvider::IDataSet> mpDataset = nullptr;
    std::vector<std::string> get_FieldNamesImpl();

public:
    DatasetWrapper();
    ~DatasetWrapper();
    DatasetWrapper(DataProvider::IDataSet* pDataset);

    // ��������� ����
    static boost::python::object enter(boost::python::object self);

    bool exit(boost::python::object type,
        boost::python::object value,
        boost::python::object traceback);

    // �� DataProvider::IDataSet
    void First();
    void Last();
    void Next();
    void Prior();
    long get_CurrentRecord();
    void put_CurrentRecord(long Value);
    long get_RecordCount();
    boost::python::object get_FieldValue(const std::string& name);
    void put_FieldValue(const std::string& name, const boost::python::object& value);
    long get_FieldCount();
    std::string DatasetWrapper::get_FieldName(long Index);
    boost::python::list get_FieldNames();
    boost::python::list get_FieldValues();
    std::string get_Filter();
    void put_Filter(const std::string& Value);
    bool get_Filtered();
    void put_Filtered(bool Value);
    void Append();
    void Delete();
    void Edit();
    void Post();
    void Cancel();
    void Insert();
    long MoveBy(long Distance);
    bool IsEmpty();
    bool Locate(const std::string& KeyFields, const boost::python::list& KeyValues, bool CaseSensitive, bool PartialKey);
    /*
    virtual HRESULT __stdcall get_DATA(VARIANT* Value) = 0;
    virtual HRESULT __stdcall put_DATA(VARIANT Value) = 0;
    */
    bool get_Eof();
    bool get_Bof();
    std::string get_IndexFieldNames();
    void put_IndexFieldNames(const std::string& Value);
    void Clear();
    bool Locate2(const std::string& KeyFields, const boost::python::list& KeyValues, bool CaseSensitive, bool PartialKey);

    // ���������������
    boost::python::list toList(const bool withHeader = false);
    std::string toStr();

};
