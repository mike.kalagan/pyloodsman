// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

/*
(C) 2020 ������ �������

������������� �������� �������� Apache, ������ 2.0 ("��������");
�� ������ ������������ ���� ���� ������ � ������������ � ���������.
�� ������ ����� ����� �������� �� ������

http://www.apache.org/licenses/LICENSE-2.0

�� ����������� �������, ����� ��� ���������������� ������������
����������������� ��� ���� ��� �� ��������� � ���������� ����������,
����������� ����������� ���������������� �� �������� ������ ��������,
��������������� "��� ����" � ����� ����� ��� ������� �������� �����������.
���������� �� �������� ������ � ������������, ����������� � �������������
����� �������� ��������, �� ������ ����� � ������ ��������.
*/

#pragma once
#include <boost/python.hpp>

inline boost::python::object VAR2PYOBJ(const CComVariant& value)
{
    switch (value.vt) {
    case VT_ARRAY:
        throw(std::exception("VT_ARRAY: Not implemented yet"));
    case VT_BLOB:
        throw(std::exception("VT_BLOB: Not implemented yet"));
    case VT_BOOL:
        return boost::python::object(value.boolVal == VARIANT_TRUE);
    case VT_BSTR: {
        CComBSTR bstrValue = value.bstrVal;
        /// \todo �������� ��� ����� ��������� boost::python::object()?;
        std::string cstr = (bstrValue.Length() == 0) ? "" : CW2A(bstrValue, CP_UTF8);
        return boost::python::str(cstr.c_str(), cstr.size());
    }
    case VT_BYREF:
        throw(std::exception("VT_BYREF not supported"));
    case VT_CY:
        /// \todo ����� �������������� C++ -> python
        return boost::python::object(value.cyVal);
    case VT_DATE:
    case VT_R8:
        return boost::python::object(value.dblVal);
    case VT_DISPATCH:
        throw(std::exception("VT_DISPATCH: Not implemented yet"));
    case VT_DECIMAL: {
        CComVariant tmpval = value;
        tmpval.ChangeType(VT_R8);
        return boost::python::object(value.dblVal);
    }
    case VT_EMPTY:
        return boost::python::object(); //-V1037
    case VT_ERROR:
        /// \todo �������� ��� ����� ���������� ����������?
        return boost::python::object(value.scode);
    case VT_INT:
    case VT_I1:
    case VT_I2:
    case VT_I4:
    case VT_I8:
        return boost::python::object(value.intVal);
    case VT_NULL:
        /// \todo ���������� ��� ����
        return boost::python::object();
    case VT_R4:
        return boost::python::object(value.fltVal);
    case VT_RECORD:
        throw(std::exception("VT_RECORD not supported"));
    case VT_UI1:
    case VT_UI2:
    case VT_UI4:
    case VT_UI8:
    case VT_UINT:
        return boost::python::object(value.uiVal);
    case VT_UNKNOWN:
        throw(std::exception("VT_UNKNOWN: Not implemented yet"));
    case VT_VARIANT: {
        CComVariant tmpvlue = *value.pvarVal;
        return VAR2PYOBJ(tmpvlue);
    }
    }
}

inline CComVariant PYOBJ2VAR(const boost::python::object& value)
{
    CComVariant result;

    if (PyList_Check(value.ptr())) {
       //Py_ssize_t size = PyList_Size(value.ptr());
        const boost::python::list lst(value);
        const boost::python::ssize_t l = boost::python::len(lst);

        CComSafeArray<VARIANT> values;

        CComSafeArrayBound bound[1];
        bound[0].SetCount(l);
        bound[0].SetLowerBound(0);

        values.Create(bound);

        for (int i = 0; i < l; ++i) {
            values.SetAt(i, PYOBJ2VAR(lst[i]));
        }
        result = values;
    } else if (PyTuple_Check(value.ptr())) {
        throw std::exception("Tuple aruments not implemented");
    } else if (PyDict_Check(value.ptr())) {
        throw std::exception("Dict aruments not implemented");
    } else {

        //const boost::python::list list = value;

        boost::python::extract<double> dblval(value);
        boost::python::extract<float> fltval(value);
        boost::python::extract<int> intval(value);
        boost::python::extract<std::string> strval(value);

        /// \todo ������� ������ - �������� ������ ���������������
        boost::python::extract<std::vector<int>> intvecval(value);


        if (strval.check()) {
            BSTR bstrVal = CComBSTR(CA2W(strval().c_str(), CP_UTF8)).Detach();
            if (bstrVal != nullptr) {
                result = bstrVal;
                result.vt = VT_BSTR;
            }
        } else if (dblval.check()) {
            result = dblval();
            result.vt = VT_R8;
        } else if (fltval.check()) {
            result = dblval();
            result.vt = VT_R4;
        } else if (intval.check()) {
            result = intval;
            result.vt = VT_R4;
        }

    }
    return result;
}
