// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

/*
(C) 2020 ������ �������

������������� �������� �������� Apache, ������ 2.0 ("��������");
�� ������ ������������ ���� ���� ������ � ������������ � ���������.
�� ������ ����� ����� �������� �� ������

http://www.apache.org/licenses/LICENSE-2.0

�� ����������� �������, ����� ��� ���������������� ������������
����������������� ��� ���� ��� �� ��������� � ���������� ����������,
����������� ����������� ���������������� �� �������� ������ ��������,
��������������� "��� ����" � ����� ����� ��� ������� �������� �����������.
���������� �� �������� ������ � ������������, ����������� � �������������
����� �������� ��������, �� ������ ����� � ������ ��������.
*/

#include "stdafx.h"
#include "config.h"
#include "dataset.h"
#include <loodsman/Olecheck.h>
#include <iostream>
#include "util.h"


DatasetWrapper::DatasetWrapper()
{
    mpDataset.Attach(loodsman::Connection::CreateDataSet());
}

DatasetWrapper::~DatasetWrapper()
{
    mpDataset.Release();
}

DatasetWrapper::DatasetWrapper(DataProvider::IDataSet* pDataset)
{
    mpDataset.Attach(pDataset);
}

boost::python::object DatasetWrapper::enter(boost::python::object self)
{
    return self;
}

bool DatasetWrapper::exit(boost::python::object type, boost::python::object value, boost::python::object traceback)
{
    mpDataset.Release();
    return false;
}

void DatasetWrapper::First()
{
    PYOLECHECK(mpDataset->First());
}

void DatasetWrapper::Last()
{
    PYOLECHECK(mpDataset->Last());
}

void DatasetWrapper::Next()
{
    PYOLECHECK(mpDataset->Next());
}

void DatasetWrapper::Prior()
{
    PYOLECHECK(mpDataset->Prior());
}

long DatasetWrapper::get_CurrentRecord()
{
    long value;
    PYOLECHECK(mpDataset->get_CurrentRecord(&value));
    return value;
}

void DatasetWrapper::put_CurrentRecord(long Value)
{
    PYOLECHECK(mpDataset->put_CurrentRecord(Value));
}

long DatasetWrapper::get_RecordCount()
{
    long value;
    PYOLECHECK(mpDataset->get_RecordCount(&value));
    return value;
}

boost::python::object DatasetWrapper::get_FieldValue(const std::string& name)
{
    CComVariant value;
    CComBSTR bstr_name(CA2W(name.c_str(), CP_UTF8));
    PYOLECHECK(mpDataset->get_FieldValue(bstr_name, &value));
    return VAR2PYOBJ(value);
}

void DatasetWrapper::put_FieldValue(const std::string& name, const boost::python::object& value)
{
    CComBSTR bsName = CA2W(name.c_str(), CP_UTF8);
    PYOLECHECK(mpDataset->put_FieldValue(bsName, PYOBJ2VAR(value)));
}

long DatasetWrapper::get_FieldCount()
{
    long value;
    PYOLECHECK(mpDataset->get_FieldCount(&value));
    return value;
}

std::string DatasetWrapper::get_FieldName(long Index)
{
    CComBSTR value;
    PYOLECHECK(mpDataset->get_FieldName(Index, &value));
    return std::string(CW2A(value, CP_UTF8));
}


std::vector<std::string> DatasetWrapper::get_FieldNamesImpl()
{
    std::vector<std::string> fieldNames;
    const long cField = this->get_FieldCount();
    for (long i = 0; i < cField; ++i) {
        CComBSTR value;
        PYOLECHECK(mpDataset->get_FieldName(i, &value));
        std::string stValue = CW2A(value);
        fieldNames.push_back(stValue);
    }
    return fieldNames;
}

boost::python::list DatasetWrapper::get_FieldNames()
{
    std::vector<std::string> fieldNames = get_FieldNamesImpl();
    boost::python::list result;
    std::for_each(fieldNames.begin(), fieldNames.end(), [&result](const std::string &name) {
        result.append(std::string(CW2A(CA2W(name.c_str()),CP_UTF8)));
    });
    return result;
}

boost::python::list DatasetWrapper::get_FieldValues()
{
    boost::python::list result;

    std::vector<std::string> fieldNames = get_FieldNamesImpl();

    boost::python::list header;
    std::for_each(fieldNames.begin(), fieldNames.end(), [this, &header](const std::string& name) {
        header.append(std::string(CW2A(CA2W(name.c_str()), CP_UTF8)));
    });

    if (!this->IsEmpty()) {
        const long cRecord = this->get_RecordCount();
        for (long i = 0; i < cRecord; ++i) {
            this->put_CurrentRecord(i + 1);
            boost::python::list line;
            std::for_each(fieldNames.begin(), fieldNames.end(), [this, &line](const std::string& name) {
                line.append(get_FieldValue(std::string(CW2A(CA2W(name.c_str()), CP_UTF8))));
                });
            result.append(line);
        }
    }

    return result;

}

std::string DatasetWrapper::get_Filter()
{
    CComBSTR value;
    PYOLECHECK(mpDataset->get_Filter(&value));
    return std::string(CW2A(value, CP_UTF8));
}

void DatasetWrapper::put_Filter(const std::string& Value)
{
    CComBSTR value(CA2W(Value.c_str(), CP_UTF8));
    PYOLECHECK(mpDataset->put_Filter(value));
}

bool DatasetWrapper::get_Filtered()
{
    VARIANT_BOOL value;
    PYOLECHECK(mpDataset->get_Filtered(&value));
    return value == VARIANT_TRUE;
}

void DatasetWrapper::put_Filtered(bool Value)
{
    PYOLECHECK(mpDataset->put_Filtered(Value ? VARIANT_TRUE : VARIANT_FALSE));
}

void DatasetWrapper::Append()
{
    PYOLECHECK(mpDataset->Append());
}

void DatasetWrapper::Delete()
{
    PYOLECHECK(mpDataset->Prior());
}

void DatasetWrapper::Edit()
{
    PYOLECHECK(mpDataset->Prior());
}

void DatasetWrapper::Post()
{
    PYOLECHECK(mpDataset->Prior());
}

void DatasetWrapper::Cancel()
{
    PYOLECHECK(mpDataset->Prior());
}

void DatasetWrapper::Insert()
{
    PYOLECHECK(mpDataset->Prior());
}

long DatasetWrapper::MoveBy(long Distance)
{
    long value;
    PYOLECHECK(mpDataset->MoveBy(Distance, &value));
    return value;
}

bool DatasetWrapper::IsEmpty()
{
    VARIANT_BOOL value;
    PYOLECHECK(mpDataset->IsEmpty(&value));
    return value == VARIANT_TRUE;
}

bool DatasetWrapper::Locate(const std::string& KeyFields, const boost::python::list& KeyValues, bool CaseSensitive, bool PartialKey)
{
    CComVariant keyValues;
    CComSafeArray<VARIANT> keys;

    if (boost::python::len(KeyValues) == 0) {
        PyErr_SetString(PyExc_LookupError, "Too few key values");
    } else if (boost::python::len(KeyValues) == 1) {
        keyValues = PYOBJ2VAR(KeyValues[0]);
    } else {
        CComSafeArrayBound bound[1];
        bound[0].SetCount(boost::python::len(KeyValues));
        bound[0].SetLowerBound(0);

        keys.Create(bound);

        for (int i = 0; i < boost::python::len(KeyValues); ++i) {
            keys.SetAt(i, PYOBJ2VAR(KeyValues[i]));
        }
        keyValues = keys;
    }

    VARIANT_BOOL retval;
    CComBSTR keyFields = CA2W(KeyFields.c_str(), CP_UTF8);
    PYOLECHECK(mpDataset->Locate(keyFields, keyValues, CaseSensitive ? VARIANT_TRUE : VARIANT_FALSE, PartialKey ? VARIANT_TRUE : VARIANT_FALSE, &retval));
    return retval == VARIANT_TRUE;
}

/*
virtual HRESULT __stdcall get_DATA(VARIANT* Value) = 0;
virtual HRESULT __stdcall put_DATA(VARIANT Value) = 0;
*/

bool DatasetWrapper::get_Eof()
{
    VARIANT_BOOL value;
    PYOLECHECK(mpDataset->get_Eof(&value));
    return value == VARIANT_TRUE;
}

bool DatasetWrapper::get_Bof()
{
    VARIANT_BOOL value;
    value = VARIANT_TRUE;
    PYOLECHECK(mpDataset->get_Bof(&value));
    return value == VARIANT_TRUE;
}

std::string DatasetWrapper::get_IndexFieldNames()
{
    CComBSTR value;
    PYOLECHECK(mpDataset->get_IndexFieldNames(&value));
    return std::string(CW2A(value, CP_UTF8));
}

void DatasetWrapper::put_IndexFieldNames(const std::string& Value)
{
    CComBSTR value(CA2W(Value.c_str(), CP_UTF8));
    PYOLECHECK(mpDataset->put_IndexFieldNames(value));
}

void DatasetWrapper::Clear()
{
    PYOLECHECK(mpDataset->Clear());
}

bool DatasetWrapper::Locate2(const std::string& KeyFields, const boost::python::list& KeyValues, bool CaseSensitive, bool PartialKey)
{
    CComVariant keyValues;
    CComSafeArray<VARIANT> keys;

    if (boost::python::len(KeyValues) == 0) {
        PyErr_SetString(PyExc_LookupError, "Too few key values");
    } else if (boost::python::len(KeyValues) == 1) {
        keyValues = PYOBJ2VAR(KeyValues[0]);
    } else {
        CComSafeArrayBound bound[1];
        bound[0].SetCount(boost::python::len(KeyValues));
        bound[0].SetLowerBound(0);
        keys.Create(bound);
        for (int i = 0; i < boost::python::len(KeyValues); ++i) {
            keys.SetAt(i, PYOBJ2VAR(KeyValues[i]));
        }
    }

    VARIANT_BOOL retval;
    CComBSTR keyFields = CA2W(KeyFields.c_str(), CP_UTF8);
    PYOLECHECK(mpDataset->Locate2(keyFields, keyValues, CaseSensitive ? VARIANT_TRUE : VARIANT_FALSE, PartialKey ? VARIANT_TRUE : VARIANT_FALSE, &retval));
    return retval == VARIANT_TRUE;
}

std::string DatasetWrapper::toStr()
{
    std::string result;

    std::vector<std::string> fieldNames = get_FieldNamesImpl();

    std::string header;
    std::for_each(fieldNames.begin(), fieldNames.end(), [this, &header](const std::string& name) {
        if (!header.empty()) header.append("\t");
        header.append(name);
    });
    result = std::move(header);
    result += "\n";
    //header.replace(header.begin(), header.end(), "", "-");
    //for (size_t i = 0; i < header.size(); ++i) result += "-";
    //result += "\n";

    const long cRecord = this->get_RecordCount();
    for (long i = 0; i < cRecord; ++i) {
        this->put_CurrentRecord(i + 1);
        std::string line;
        std::for_each(fieldNames.begin(), fieldNames.end(), [this, &line](const std::string& name) {
            if (!line.empty()) line += "\t";
            CComVariant value;
            CComBSTR bstr_name(name.c_str());
            PYOLECHECK(mpDataset->get_FieldValue(bstr_name, &value));
            if ((value.vt != VT_NULL) && (value.vt != VT_EMPTY)) {
                value.ChangeType(VT_BSTR);
                line += CW2A(CComBSTR(value.bstrVal));
            }
        });
        result += line;
        result += "\n";
    }

    result = CW2A(CA2W(result.c_str()), CP_UTF8);
    return result;

}

boost::python::list DatasetWrapper::toList(const bool withHeader)
{
    boost::python::list result;

    std::vector<std::string> fieldNames = get_FieldNamesImpl();

    if (withHeader) {
        boost::python::list header;
        std::for_each(fieldNames.begin(), fieldNames.end(), [this, &header](const std::string& name) {
            header.append(std::string(CW2A(CA2W(name.c_str()), CP_UTF8)));
        });
        result.append(header);
    }

    if (!this->IsEmpty()) {
        const long cRecord = this->get_RecordCount();
        for (long i = 0; i < cRecord; ++i) {
            this->put_CurrentRecord(i + 1);
            boost::python::list line;
            std::for_each(fieldNames.begin(), fieldNames.end(), [this, &line](const std::string& name) {
                line.append(get_FieldValue(std::string(CW2A(CA2W(name.c_str()), CP_UTF8))));
            });
            result.append(line);
        }
    }

    return result;

}