/*
(C) 2017 ����

������������� �������� �������� Apache, ������ 2.0 ("��������");
�� ������ ������������ ���� ���� ������ � ������������ � ���������.
�� ������ ����� ����� �������� �� ������

http://www.apache.org/licenses/LICENSE-2.0

�� ����������� �������, ����� ��� ���������������� ������������
����������������� ��� ���� ��� �� ��������� � ���������� ����������,
����������� ����������� ���������������� �� �������� ������ ��������,
��������������� "��� ����" � ����� ����� ��� ������� �������� �����������.
���������� �� �������� ������ � ������������, ����������� � �������������
����� �������� ��������, �� ������ ����� � ������ ��������.
*/

#pragma once

#include <algorithm>
#include <vector>
#include <map>

#include <comdef.h>
#include <cassert>
#include <iostream>
#include "olecheck.h"

#ifdef CUSTOM_IDATASET
namespace DataProvider
{
    struct __declspec(uuid("aae5a095-1616-4aa6-a14d-d2c973191e79"))
        IDataSet : IDispatch
    {
        //
        // Raw methods provided by interface
        //

        virtual HRESULT __stdcall First() = 0;
        virtual HRESULT __stdcall Last() = 0;
        virtual HRESULT __stdcall Next() = 0;
        virtual HRESULT __stdcall Prior() = 0;
        virtual HRESULT __stdcall get_CurrentRecord(
            /*[out,retval]*/ long* Value) = 0;
        virtual HRESULT __stdcall put_CurrentRecord(
            /*[in]*/ long Value) = 0;
        virtual HRESULT __stdcall get_RecordCount(
            /*[out,retval]*/ long* Value) = 0;
        virtual HRESULT __stdcall get_FieldValue(
            /*[in]*/ BSTR Name,
            /*[out,retval]*/ VARIANT* Value) = 0;
        virtual HRESULT __stdcall put_FieldValue(
            /*[in]*/ BSTR Name,
            /*[in]*/ VARIANT Value) = 0;
        virtual HRESULT __stdcall get_FieldCount(
            /*[out,retval]*/ long* Value) = 0;
        virtual HRESULT __stdcall get_FieldName(
            /*[in]*/ long Index,
            /*[out,retval]*/ BSTR* Value) = 0;
        virtual HRESULT __stdcall get_Filter(
            /*[out,retval]*/ BSTR* Value) = 0;
        virtual HRESULT __stdcall put_Filter(
            /*[in]*/ BSTR Value) = 0;
        virtual HRESULT __stdcall get_Filtered(
            /*[out,retval]*/ VARIANT_BOOL* Value) = 0;
        virtual HRESULT __stdcall put_Filtered(
            /*[in]*/ VARIANT_BOOL Value) = 0;
        virtual HRESULT __stdcall Append() = 0;
        virtual HRESULT __stdcall Delete() = 0;
        virtual HRESULT __stdcall Edit() = 0;
        virtual HRESULT __stdcall Post() = 0;
        virtual HRESULT __stdcall Cancel() = 0;
        virtual HRESULT __stdcall Insert() = 0;
        virtual HRESULT __stdcall MoveBy(
            /*[in]*/ long Distance,
            /*[out,retval]*/ long* result) = 0;
        virtual HRESULT __stdcall IsEmpty(
            /*[out,retval]*/ VARIANT_BOOL* Value) = 0;
        virtual HRESULT __stdcall Locate(
            /*[in]*/ BSTR KeyFields,
            /*[in]*/ VARIANT KeyValues,
            /*[in]*/ VARIANT_BOOL CaseSensitive,
            /*[in]*/ VARIANT_BOOL PartialKey,
            /*[out,retval]*/ VARIANT_BOOL* Value) = 0;
        virtual HRESULT __stdcall get_DATA(
            /*[out,retval]*/ VARIANT* Value) = 0;
        virtual HRESULT __stdcall put_DATA(
            /*[in]*/ VARIANT Value) = 0;
        virtual HRESULT __stdcall get_Eof(
            /*[out,retval]*/ VARIANT_BOOL* Value) = 0;
        virtual HRESULT __stdcall get_Bof(
            /*[out,retval]*/ VARIANT_BOOL* Value) = 0;
        virtual HRESULT __stdcall get_IndexFieldNames(
            /*[out,retval]*/ BSTR* Value) = 0;
        virtual HRESULT __stdcall put_IndexFieldNames(
            /*[in]*/ BSTR Value) = 0;
        virtual HRESULT __stdcall Clear() = 0;
        virtual HRESULT __stdcall Locate2(
            /*[in]*/ BSTR KeyFields,
            /*[in]*/ VARIANT KeyValues,
            /*[in]*/ VARIANT_BOOL CaseSensitive,
            /*[in]*/ VARIANT_BOOL PartialKey,
            /*[out,retval]*/ VARIANT_BOOL* Value) = 0;
    };
}
#else
#import "libid:76AA08AD-DEDA-40CB-962B-24440A3E96AD" version("1.0") lcid("0"), named_guids, raw_interfaces_only, auto_search // IDataSet
#endif

namespace loodsman
{
    // { 392BA982-A82F-44AB-BA8A-69BE25199F73 }
    extern "C" const GUID __declspec(selectany) CLSID_MainSystem =
    { 0x392ba982,0xa82f,0x44ab,{ 0xba,0x8a,0x69,0xbe,0x25,0x19,0x9f,0x73 } };

    const LPCTSTR LOODSMAN_REG_KEY_NAME = TEXT("Software\\ASCON\\Loodsman");

    extern DataProvider::IDataSet *CreateDataSet();

    class Connection
    {
        const LPCTSTR DEFAULT_APP_SERVER_NAME = TEXT("SAP2");

        CComDispatchDriver mAppServer;
        CString mHost, mDBName, mLoodsmanPath, mLoodsmanCommonPath;

        LONG mToFolderId = 0;

    public:
        class Error
        {
            std::wstring mWhat;
        public:
            Error(LPCWSTR msg) : mWhat(msg) {}
            LPCWSTR what() const { return mWhat.c_str(); }
        };

        static DataProvider::IDataSet *CreateDataSet()
        {
            typedef HRESULT(__stdcall *LPStubCreateDatSet)(
                DataProvider::IDataSet** ppDataSet);

            static LPStubCreateDatSet StubCreateDataSet = nullptr;
            if (StubCreateDataSet == nullptr) {
                CRegKey loodsman;
                LSTATUS status = loodsman.Open(HKEY_LOCAL_MACHINE,
                    LOODSMAN_REG_KEY_NAME, KEY_READ | KEY_WOW64_32KEY);

                ATLASSERT(status == ERROR_SUCCESS);
                ATLASSERT(loodsman.m_hKey != 0);

                TCHAR value[MAX_PATH] = { 0 };
                ULONG cvalue = MAX_PATH;

                loodsman.QueryStringValue(TEXT("Path"), value, &cvalue);
#ifdef UNICODE
                wcscat(value, TEXT("Client\\LUStub.dll"));
#else
                strcat(value, TEXT("Client\\LUStub.dll"));
#endif

                HMODULE hmodLUStub = LoadLibrary(value);
                if (hmodLUStub == NULL) {
                    hmodLUStub = LoadLibrary(TEXT("LUStub.dll"));
                }

                if (hmodLUStub != NULL) {
                    StubCreateDataSet = reinterpret_cast<LPStubCreateDatSet>(
                        GetProcAddress(hmodLUStub, "StubCreateDataSet"));
                }

            }

            ATLASSERT(StubCreateDataSet != nullptr);
            DataProvider::IDataSet *pDataSet;
            OLECHECK(StubCreateDataSet(&pDataSet));

            if (pDataSet == nullptr) {
                throw oleerror(E_FAIL, "pDataSet == nullptr");
            }

            return pDataSet; // � ���� ����� ������� ������ ��� ����� �������.
        }


        Connection(const CString &host):
            mHost(host)
        {
            LPVOID *pvReserved = nullptr;
            CoInitialize(pvReserved);
            CoInitializeSecurity(
                NULL,
                -1,
                NULL,
                NULL,
                RPC_C_AUTHN_LEVEL_NONE,   /* 0 */
                RPC_C_IMP_LEVEL_DELEGATE, /* 4 */
                NULL,
                0,
                NULL
            );


            CRegKey loodsman;
            LSTATUS status = loodsman.Open(HKEY_LOCAL_MACHINE,
                LOODSMAN_REG_KEY_NAME, KEY_READ | KEY_WOW64_32KEY);

            ATLASSERT(status == ERROR_SUCCESS);
            ATLASSERT(loodsman.m_hKey != 0);

            TCHAR value[MAX_PATH] = { 0 };
            ULONG cvalue;

            cvalue = MAX_PATH;
            if (loodsman.QueryStringValue(TEXT("Path"), value,
                &cvalue) == ERROR_SUCCESS) {
                mLoodsmanPath = value;
            }

            cvalue = MAX_PATH;
            if (loodsman.QueryStringValue(TEXT("CommonPath"),
                value, &cvalue) == ERROR_SUCCESS) {
                mLoodsmanCommonPath = value;
                mLoodsmanCommonPath += L"\\";
            }

            cvalue = MAX_PATH;

            if (mHost.IsEmpty()) {
                if (loodsman.QueryStringValue(TEXT("SP"), value,
                    &cvalue) == ERROR_SUCCESS) {
                    mHost = (cvalue > 0) ? value : DEFAULT_APP_SERVER_NAME;
                } else {
                    mHost = DEFAULT_APP_SERVER_NAME;
                }
            }

            MULTI_QI mq[1] = { 0 };
            mq[0].pIID = &IID_IDispatch;
            mq[0].pItf = NULL;
            mq[0].hr = S_OK;

            loodsman.Close();

            const auto &name = CT2W(mHost);

            COSERVERINFO csi = { 0, name,
                NULL, 0 };

            //CComVariant ret, err, value;

            VARIANT errMsgRef, errCodeRef;
            CComVariant errMsg, errCode;

            errMsgRef.vt = VT_BYREF | VT_VARIANT;
            errCodeRef.vt = VT_BYREF | VT_VARIANT;

            errMsgRef.pvarVal = &errMsg;
            errCodeRef.pvarVal = &errCode;

            OLECHECK(CoCreateInstanceEx(CLSID_MainSystem, NULL,
                CLSCTX_REMOTE_SERVER, &csi, 1, mq));


            mAppServer.Attach(reinterpret_cast<IDispatch*>(mq[0].pItf));
        }

        ~Connection()
        {
            CoUninitialize();
        }

        LPCTSTR GetAppServerName()
        {
            return mHost;
        }

        LPCTSTR GetLoodsmanPath()
        {
            return mLoodsmanPath;
        }

        LPCTSTR GetLoodsmanCommonPath()
        {
            return  mLoodsmanCommonPath;
        }

        LONG GetPersonalFolderId(LPTSTR DBName)
        {
            if ((mToFolderId == 0)&&(mDBName != DBName)) {
                CRegKey loodsman;
                DWORD dwValue;
                if ((loodsman.Open(HKEY_CURRENT_USER,
                    TEXT("Software\\Skbm\\Loodsman\\ToFolder"),
                    KEY_READ | KEY_WOW64_32KEY) == ERROR_SUCCESS) &&
                    (loodsman.m_hKey != 0) && (loodsman.QueryDWORDValue(DBName,
                        dwValue) == ERROR_SUCCESS)) {
                    mToFolderId = dwValue;
                    mDBName = DBName;
                }
            }
            return mToFolderId;
        }

        IDispatch *ptr()
        {
            return mAppServer;
        }

        void Invoke(const LPCTSTR method,
            const std::vector<CComVariant> &args,
            CComVariant *retVal = NULL)
        {
            std::vector<VARIANT> checkedargs;

            VARIANT errMsgRef, errCodeRef;
            VARIANT errMsg = { 0 };
            VARIANT errCode = { 0 };

            errMsgRef.vt = VT_BYREF | VT_VARIANT;
            errCodeRef.vt = VT_BYREF | VT_VARIANT;

            errMsgRef.pvarVal = &errMsg;
            errCodeRef.pvarVal = &errCode;

            checkedargs.push_back(errMsgRef);
            checkedargs.push_back(errCodeRef);

            std::for_each(args.rbegin(), args.rend(),
                [&checkedargs](const CComVariant &value) {
                checkedargs.push_back(static_cast<VARIANT>(value));
            });

            VARIANT ret = { 0 };

            OLECHECK(mAppServer.InvokeN(CT2W(method), checkedargs.data(),
                checkedargs.size(), &ret));

            if (errCode.intVal != 0) {
                throw Error(CComBSTR(errMsg.bstrVal));
            }

            if ((ret.vt != VT_EMPTY) && (ret.vt & VT_ARRAY)) {
                CComPtr<DataProvider::IDataSet> ds = CreateDataSet();
                ds->put_DATA(ret);
                CComDispatchDriver dsdisp = ds;
                *retVal = dsdisp;
            } else {
                if (retVal != nullptr) {
                   OLECHECK(retVal->Copy(&ret));
                }
            }

        }
    };

}