/*
(C) 2017 СКБМ

Лицензировано согласно Лицензии Apache, Версия 2.0 ("Лицензия");
вы можете использовать этот файл только в соответствии с Лицензией.
Вы можете найти копию Лицензии по адресу

http://www.apache.org/licenses/LICENSE-2.0

За исключением случаев, когда это регламентировано существующим
законодательством или если это не оговорено в письменном соглашении,
программное обеспечение распространяемое на условиях данной Лицензии,
предоставляется "КАК ЕСТЬ" и любые явные или неявные ГАРАНТИИ ОТВЕРГАЮТСЯ.
Информацию об основных правах и ограничениях, применяемых к определенному
языку согласно Лицензии, вы можете найти в данной Лицензии.
*/

#pragma once
#include <comdef.h>

#ifdef HAS_BOOST
#include <boost/format.hpp>
#endif HAS_BOOST

#include <string>

class oleerror
{
    HRESULT mHr;
    std::wstring mWhat;
public:
    oleerror(const HRESULT hr, const LPCSTR statment) : mHr(hr)
    {
        CComPtr<IErrorInfo> errInfo;
        GetErrorInfo(0, &errInfo);
        if (errInfo != nullptr) {
            CComBSTR source, description;
            errInfo->GetSource(&source);
            errInfo->GetDescription(&description);
            _com_error err(hr, errInfo);
#ifdef HAS_BOOST
            mWhat = (boost::wformat(L"0x%X: %s - %s: %s") % mHr
                % err.ErrorMessage()
                % static_cast<LPWSTR>(source)
                % static_cast<LPWSTR>(description)).str();
#else 
            mWhat = CT2W(err.ErrorMessage());
#endif
        } else {
            _com_error err(hr);
#ifdef HAS_BOOST
            mWhat = (boost::wformat(L"0x%X: %s") % mHr
                % err.ErrorMessage()).str();
#else
            mWhat = CT2W(err.ErrorMessage());
#endif

        }
    }

    LPCWSTR what() const
    {
        return mWhat.c_str();
    }
    const HRESULT hr() const
    {
        return mHr;
    }
};

#define OLECHECK(statement)\
{\
    HRESULT hr = statement;\
    if (FAILED(hr)) throw oleerror(hr, #statement);\
}
