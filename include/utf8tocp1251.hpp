/// \file utf8tocp1251.hpp \briefe �������������� �� ��������� UTF8 � CP1251

#pragma once
#ifndef UTF8TO1251_H
#define UTF8TO1251_H
#include <string>
#include <codecvt>

inline std::string utf8to1251(std::string utf8_str)
{
    std::wstring_convert<std::codecvt_utf8<wchar_t>> wconv;

    std::wstring wstr = wconv.from_bytes(utf8_str.c_str());
    std::vector<char> buf(wstr.size());
    std::use_facet<std::ctype<wchar_t>>(std::locale(".1251")).narrow(wstr.data(), wstr.data() + wstr.size(), '?', buf.data());
    return std::string(buf.data(), buf.size());
}
#endif
