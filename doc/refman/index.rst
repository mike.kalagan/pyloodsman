.. PyLoodsman documentation master file, created by
   sphinx-quickstart on Fri Aug  2 10:38:03 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Добро пожаловать в Справочное руководство PyLoodsman!
=====================================================

Оглавление:

.. toctree::
   :maxdepth: 2

   connection
   dataset

Таблицы и указатели
===================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

